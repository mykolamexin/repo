//import React from 'react';
import { Component } from "react";
import calculatorsData from './calculator.json';
import styles from '../components/Calculator.module.css';

class MyFirstComponent extends Component  {
    state = {
        curentTariff: calculatorsData.ALLSIMPLE,
        result: "",
    }

    handleClick = (element) => {
        element.preventDefault();
        const  name  = element.target.id;

        console.log(name,"Click")

        switch (name) {
            case 'ALLSIMPLE': this.setState({...this.state, curentTariff: calculatorsData.ALLSIMPLE});
                break;
            case 'FREE': this.setState({...this.state, curentTariff: calculatorsData.FREE});
                break;
            case 'IP': this.setState({...this.state, result: this.state.curentTariff.IP});
                break;
            case 'OOO': this.setState({...this.state, result: this.state.curentTariff.OOO});
                break;
            default: return;
        }
    }

    render() {
         const keysTariff = Object.keys(calculatorsData);

        return (
            <>
                <form>
                <div className={styles.buttonBlock}>
                    <button className={styles.buttonLeft} id='IP' onClick={this.handleClick}><span className={styles.buttonStylesText}>ИП</span></button>
                    <button className={styles.buttonRight} id="OOO" onClick={this.handleClick}><span className={styles.buttonStylesText}>ООО</span></button>
                </div>
                
                <div className={styles.buttonBlockSmall}>
                    <button className={styles.buttonLeftSmall} id="USN_d" value={this.state.result.USN_d} onClick={this.handleClick}><span className={styles.buttonStylesText}>УСН-Д</span></button>
                    <button className={styles.buttonCenterSmall} id="USN_d_r" value={this.state.result.USN_d_r} onClick={this.handleClick}><span className={styles.buttonStylesText}>УСН Д-Р</span></button>
                    <button className={styles.buttonRightSmall} id="OSNO" value={this.state.result.OOO} onClick={this.handleClick}><span className={styles.buttonStylesText}>ОСНО</span></button>
                </div>    

                <div className={styles.blockTariffPrice}>
                    <div className={styles.smallBlockTariffPrice}>
                        <p className={styles.blockTariffTitle}>Примерная цена:</p>
                        <span className={styles.priceTariff}>2 500 ₽</span></div>
                </div>

                <ul>Тарифы
                 
                    <li className={styles.blockTariff} id={keysTariff[0]} key={keysTariff[0]} onClick={this.handleClick}>Все просто</li> 
                    <li className={styles.blockTariff} id={keysTariff[1]} key={keysTariff[1]} onClick={this.handleClick}>Свободный</li>               
                </ul>
             
                </form>
            </>
            );
    }

}

export default MyFirstComponent;
